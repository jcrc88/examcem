﻿CREATE TABLE [dbo].[Customers]
(
	[CustomerId] INT NOT NULL PRIMARY KEY, 
	[CompanyName] NCHAR(100) NULL, 
	[ContactName] NCHAR(100) NULL, 
	[ContactTitle] NCHAR(50) NULL, 
	[Address] NCHAR(150) NULL, 
	[City] NCHAR(50) NULL, 
	[Region] NCHAR(10) NULL, 
	[PostalCode] NCHAR(5) NULL, 
	[Country] NCHAR(20) NULL
)
