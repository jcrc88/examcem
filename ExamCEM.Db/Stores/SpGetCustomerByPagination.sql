﻿CREATE PROCEDURE SpGetCustomerByPagination
       @CompanyNameFilter VARCHAR(150),
       @PageSize INT,
       @PageCurrent INT,
	   @Total INT OUTPUT
AS
       SET NOCOUNT ON;

	   DECLARE @Offset INT = @PageSize * (@PageCurrent - 1);

       SELECT CustomerID, 
			RTRIM(CompanyName) AS CompanyName, 
			RTRIM(ContactName) AS ContactName, 
			RTRIM([Address]) AS [Address]
       FROM Customers
       WHERE CompanyName = @CompanyNameFilter
	   ORDER BY CustomerId
       OFFSET @Offset ROWS
       FETCH NEXT @PageSize ROWS ONLY;

	   SELECT @Total = COUNT(CustomerId) FROM Customers
       WHERE CompanyName = @CompanyNameFilter;

	   RETURN
GO

