# Examen Cristiam Espinoza

## Ejecutar Db Scripts

### Stored Procedure

    ExamCEM.Db\Stores\SpGetCustomerByPagination.sql

### Create Table
    ExamCEM.Db\Tables\Customer.sql

### Datos Dummies
    ExamCEM.Db\Inserts\Customers.sql

## Cambiar configuración

Cambiar la cadena de conexión a base de datos de ser necesario, actualmente apunta a repositorio local.
