﻿using ExamCEM.Entites.Config;
using ExamCEM.Entites.Domain;
using ExamCEM.Entites.Dto;
using ExamCEM.Entites.Repo;
using ExamCEM.Models.Sql;
using Microsoft.Extensions.Options;
using System.Collections.Generic;

namespace ExamCEM.Domain
{
    public class CustomerDomain : ICustomerDomain
    {
        readonly ICustomerRepo _repo;
        readonly PaginationConfig _config;

        public CustomerDomain(IOptions<PaginationConfig> config, ICustomerRepo repo)
        {
            _config = config.Value;
            _repo = repo;
        }

        public CustomerPagination GetCustomersByCompanyName(string companyName, int page, int? size)
        {
            ICollection<Customer> customers = null;
            int totalSize = size ?? _config.DefaultPageSize;
            if (totalSize <= 0)
            {
                totalSize = _config.DefaultPageSize;
            }

            customers = _repo.GetByNamePagination(companyName, page, totalSize, out int totalResults);

            if (customers == null) return null;

            return new CustomerPagination(customers)
            {
                CurrentPage = page,
                SizePage = totalSize,
                TotalRows = totalResults,
            };
        }
    }
}
