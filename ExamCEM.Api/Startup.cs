﻿using ExamCEM.Domain;
using ExamCEM.Entites.Config;
using ExamCEM.Entites.Domain;
using ExamCEM.Entites.Repo;
using ExamCEM.Repository.Sql;
using ExamCEM.Utils.Api.Filters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Reflection;

namespace ExamCEM.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options =>
            {
                // Enables always model validation
                options.Filters.Add(typeof(ValidateModelStateFilter));
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // Configuration Settings
            services.AddOptions();

            var defaultDbConnection = Configuration.GetConnectionString("DbConnection");
            var paginationConfig = new PaginationConfig();
            Configuration.GetSection("PaginationConfig").Bind(paginationConfig);

            services.Configure<SqlConfig>((options) => options.DbConnectionString = defaultDbConnection);
            //services.Configure<PaginationConfig>((pagination) => pagination = paginationConfig);
            services.Configure<PaginationConfig>(Configuration.GetSection("PaginationConfig"));

            // DI
            services.AddTransient<ICustomerDomain, CustomerDomain>();
            services.AddTransient<ICustomerRepo, CustomerRepo>();


            // Register Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info()
                {
                    Title = "JCRC Exam",
                    Version = "v1",
                });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                // Enable Swagger (JSON)
                app.UseSwagger();
                // Enable swagger UI
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "JCRC Exam");
                    c.RoutePrefix = "swagger";
                });
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
