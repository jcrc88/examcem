﻿using ExamCEM.Entites.Domain;
using ExamCEM.Entites.Dto;
using ExamCEM.Utils.Filters;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace ExamCEM.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        readonly ICustomerDomain _domain;

        public CustomerController(ICustomerDomain domain)
        {
            _domain = domain;
        }

        /// <summary>
        /// Return customers by Company Name
        /// </summary>
        /// <param name="companyName">Company name</param>
        /// <param name="page">Current page</param>
        /// <param name="size">Total </param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ValidateQueryParametersFilter]
        public IActionResult GetByFilter(
            [Required]string companyName,
            [Required, Range(0, int.MaxValue, ErrorMessage = "Valor debe ser mayor a cero.")]int page,
            int? size)
        {
            CustomerPagination result = _domain.GetCustomersByCompanyName(companyName, page, size);

            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }
    }
}