﻿SELECT CustomerID, CompanyName, ContactName, [Address]
       FROM Customers
       WHERE CompanyName = 'Kimia'
	   ORDER BY CustomerId
       OFFSET 2 ROWS
       FETCH NEXT 2 ROWS ONLY;

DECLARE @Total INT;	  
EXEC SpGetCustomerByPagination 'Kimia', 2, 1, @Total OUTPUT;
SELECT @Total;


ALTER PROCEDURE SpGetCustomerByPagination
       @CompanyNameFilter VARCHAR(150),
       @PageSize INT,
       @PageCurrent INT,
	   @Total INT OUTPUT
AS
       SET NOCOUNT ON;

	   DECLARE @Offset INT = @PageSize * (@PageCurrent - 1);

       SELECT CustomerID, CompanyName, ContactName, [Address]
       FROM Customers
       WHERE CompanyName = @CompanyNameFilter
	   ORDER BY CustomerId
       OFFSET @Offset ROWS
       FETCH NEXT @PageSize ROWS ONLY;

	   SELECT @Total = COUNT(CustomerId) FROM Customers
       WHERE CompanyName = @CompanyNameFilter;

	   RETURN
GO
