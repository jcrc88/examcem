﻿using ExamCEM.Entites.Config;
using ExamCEM.Entites.Repo;
using ExamCEM.Models.Sql;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ExamCEM.Repository.Sql
{
    public class CustomerRepo : ICustomerRepo
    {
        readonly SqlConfig _settings;

        public CustomerRepo(IOptions<SqlConfig> settings)
        {
            _settings = settings.Value;
        }

        public ICollection<Customer> GetByNamePagination(string companyName, int page, int size, out int totalResults)
        {
            IList<Customer> customers = null;
            using (SqlConnection conn = new SqlConnection(_settings.DbConnectionString))
            {
                SqlCommand cmd = new SqlCommand(SqlCommands.SpCustomersPagination, conn)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.AddWithValue("@CompanyNameFilter", companyName);
                cmd.Parameters.AddWithValue("@PageSize", size);
                cmd.Parameters.AddWithValue("@PageCurrent", page);
                SqlParameter parameter = new SqlParameter("@Total", SqlDbType.Int, 0)
                {
                    Direction = ParameterDirection.Output
                }; //cmd.Parameters.Add("@Total", SqlDbType.Int);
                cmd.Parameters.Add(parameter);

                conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    customers = new List<Customer>();

                    while (reader.Read())
                    {
                        Customer customer = new Customer()
                        {
                            CustomerId = Convert.ToInt32(reader["CustomerId"]),
                            CompanyName = reader["CompanyName"].ToString(),
                            ContactName = reader["ContactName"].ToString(),
                            Address = reader["Address"].ToString()
                        };

                        customers.Add(customer);
                    }
                }
                reader.Close();
                conn.Close();

                totalResults = Convert.ToInt32(cmd.Parameters["@Total"].Value);
            }

            return customers;
        }
    }
}
