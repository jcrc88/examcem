﻿namespace ExamCEM.Models.Sql
{
    /// <summary>
    /// Información del cliente
    /// </summary>
    public class Customer
    {
        public int CustomerId { get; set; }

        public string CompanyName { get; set; }

        public string ContactName { get; set; }

        public string Address { get; set; }
    }
}
