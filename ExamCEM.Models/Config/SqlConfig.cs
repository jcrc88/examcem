﻿namespace ExamCEM.Entites.Config
{
    /// <summary>
    /// Database settings
    /// </summary>
    public class SqlConfig
    {
        /// <summary>
        /// Default Connection string
        /// </summary>
        public string DbConnectionString { get; set; }
    }
}
