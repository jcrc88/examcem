﻿namespace ExamCEM.Entites.Config
{
    /// <summary>
    /// Pagination configuration.
    /// </summary>
    public class PaginationConfig
    {
        /// <summary>
        /// Default pagination size
        /// </summary>
        public int DefaultPageSize { get; set; }
    }
}
