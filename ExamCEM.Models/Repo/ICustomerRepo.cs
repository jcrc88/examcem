﻿using ExamCEM.Models.Sql;
using System.Collections.Generic;

namespace ExamCEM.Entites.Repo
{
    public interface ICustomerRepo
    {
        ICollection<Customer> GetByNamePagination(string companyName, int page, int size, out int totalResults);
    }
}
