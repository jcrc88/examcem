﻿using ExamCEM.Models.Sql;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ExamCEM.Entites.Dto
{
    public class CustomerPagination
    {
        [JsonProperty(PropertyName = "customers")]
        public ICollection<Customer> Customers;

        [JsonProperty(PropertyName = "totalResults")]
        public int TotalRows { get; set; }

        public int TotalPages
        {
            get
            {
                if (TotalRows == 0)
                {
                    return 0;
                }
                decimal total = TotalRows / (decimal)SizePage;
                return (int)Math.Ceiling(total);
            }
        }

        [JsonProperty(PropertyName = "currentPage")]
        public int CurrentPage { get; set; }

        [JsonProperty(PropertyName = "sizePage")]
        public int SizePage { get; set; }

        public CustomerPagination(ICollection<Customer> customers)
        {
            Customers = customers;
        }
    }
}
