﻿using ExamCEM.Entites.Dto;

namespace ExamCEM.Entites.Domain
{
    public interface ICustomerDomain
    {
        /// <summary>
        /// Get cutomers paginated
        /// </summary>
        /// <param name="companyName"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        CustomerPagination GetCustomersByCompanyName(string companyName, int page, int? size);
    }
}
